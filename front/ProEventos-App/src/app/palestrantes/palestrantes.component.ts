import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-palestrantes',
  templateUrl: './palestrantes.component.html',
  styleUrls: ['./palestrantes.component.scss']
})
export class PalestrantesComponent implements OnInit {

  public palestrantes: any;

  constructor(private http:HttpClient ) { }

  ngOnInit(): void {
    this.getPalestrantes();
  }

  public getPalestrantes(): void{

    this.palestrantes = [
      {
        Nome: "Emerson",
        Curso: "Angular",
        Telefone: "(92) 9999-99990"
      },
      {
        Nome: "Ricardo",
        Curso: "Yii",
        Telefone: "(92) 9999-99990"
      },
      {
        Nome: "Gabriel",
        Curso: "React",
        Telefone: "(92) 9999-9999"
      }
    ]
  }

}
